""" Parse common passwords data from the table available at `passwordrandom
<https://www.passwordrandom.com/most-popular-passwords>`_ site.
"""

import os
import json
from urllib import request
from datetime import datetime

from bs4 import BeautifulSoup


URL = 'https://www.passwordrandom.com/most-popular-passwords/page/{}'
DATA_KEYS = ['rating', 'name', 'md5', 'length', 'lower_case', 'upper_case', 'numbers']


def get_current_path():
    return os.path.dirname(os.path.abspath(__file__))


def check_finished(page_number):
    with open(os.path.join(get_current_path(), 'input.txt'), 'r') as input_file:
        is_pages_finished = page_number > int(input_file.read())
    return is_pages_finished


def extract_data(row):
    pwd_data = dict(zip(DATA_KEYS, map(lambda td: td.get_text(), row.find_all('td')[:-1])))
    pwd_data['lower_case'] = int(pwd_data['lower_case'])
    pwd_data['upper_case'] = int(pwd_data['upper_case'])
    pwd_data['numbers'] = int(pwd_data['numbers'])
    return pwd_data


def write_data(data):
    with open(os.path.join(get_current_path(), '{}.json'.format(datetime.now())), 'w') as json_file:
        json.dump(data, json_file)


def log_stat(arr, name):
    with open(os.path.join(get_current_path(), 'parser.log'), 'a') as log_file:
        def log(string):
            log_file.write('\n[{}] {}'.format(datetime.now(), string))
        log(name)
        log('    avg: {:.2f}'.format(sum(arr) / len(arr)))
        log('    min: {:.2f}'.format(min(arr)))
        log('    max: {:.2f}\n'.format(max(arr)))


def parse():
    """ Main function to parse password data from website

    :return: list of data about passwords
    :rtype: list[dict]
    """
    pwds_data = []
    lc_values = []
    uc_values = []
    n_values = []
    page_number = 1
    while True:
        if check_finished(page_number):
            break
        pwd_req = request.Request(URL.format(page_number), headers={'User-Agent': "Password Scrapper"})
        with request.urlopen(pwd_req) as pwd_res:
            page = BeautifulSoup(pwd_res.read(), 'html.parser')
            all_trs = page.find_all('tr')[1:]  # Skip first row because it is header
            for row in all_trs:
                pwd_data = extract_data(row)
                lc_values.append(pwd_data['lower_case'])
                uc_values.append(pwd_data['upper_case'])
                n_values.append(pwd_data['numbers'])
                pwds_data.append(pwd_data)
        page_number += 1
    log_stat(lc_values, 'Lower case')
    log_stat(uc_values, 'Upper case')
    log_stat(n_values, 'Numbers')
    return pwds_data


if __name__ == '__main__':
    write_data(parse())
